import express from 'express';
import cors from 'cors'
import React from 'react'
import { renderToString } from 'react-dom/server';
import serialize from 'serialize-javascript';

import App from '../shared/App'

const app = express();

app.use(express.static('public'))
app.use(cors());

app.get('*', (req, res, next) => {
  const data = "Henloooo "
  const markUp = renderToString(<App data={data}/>)

  res.send(`
    <!DOCTYPE html>
    <html>
      <head>
        <title>ReactJS SRR</title>
      </head>
      <body>
        <div id="app">${markUp}</div>
        <script src="/bundle.js" defer></script>
        <script>window.__INITIAL_DATA__ = ${serialize(data)}</script>
      </body>
    </html>
  `)
})

app.listen(3000, () => {
  console.log('Server is listening on PORT: 3000')
})